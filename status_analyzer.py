# -*- coding: utf-8 -*-
"""
Created on Sat Jan 27 19:16:51 2018

@author: enovi
"""

import nltk

#status analyzer
#based on sentiment analyzer
#using list of high-status words to analyze social status of company

def main():
    choice = True
    article_text = ''
    c = 'y'
    print('Welcome to Article Status Analyzer!')
    while choice == True:
         c = input('Analyze an article y/n? ')
         if c == 'y':
             article_text = get_text()
             status_sentiment(article_text)
             choice = True
         elif c == 'n':
             choice = False

def get_text():
    article_text = 'empty_string'
    print('This program analyzes the social status of a message.')
    article_text = str(input('Enter the message here: '))
    return article_text

def status_sentiment(article_text):
    status_words = 0
    status_score = 0
    
    status_list = ['acclaim', 'aesthetic', 'allude', 'appall', 'arguably', 'beautiful',
                   'bridging', 'calculation', 'canvas', 'capstone', 'chaperone', 'commemorate',
                   'commentary', 'commentator', 'complement', 'complicate', 'consistency',
                   'continuation', 'convey', 'culinary', 'curator', 'delve', 'devastate',
                   'divulge', 'duration', 'dynamic', 'emerge', 'emphasize', 'empower',
                   'enormous', 'essential', 'exhibit', 'exhilarating', 'expansive', 'fascinate',
                   'ferment', 'finery', 'flexibility', 'former', 'fortunate', 'frivolous',
                   'fulfill', 'fuzzy', 'genuine', 'gratify', 'hesitancy', 'imbalance', 'influx',
                   'infuse', 'innate', 'innumerable', 'inquiries', 'inquisitive', 'inspire',
                   'integral', 'intend', 'intense', 'interpersonal', 'interpret', 'introspect',
                   'invaluable', 'limitless', 'local', 'measly', 'motivating', 'necessary',
                   'necessitate', 'palette', 'passionate', 'pastime', 'perspective', 'precise',
                   'primarily', 'profound', 'prohibitive', 'proliferate', 'reaffirm',
                   'recreation', 'recyclable', 'redefining', 'reflection', 'resolve', 'revere',
                   'revering', 'rigorous', 'scholastic', 'sculpt', 'shockingly', 'sop',
                   'splash', 'substantive', 'succinct', 'supercilious', 'sustain', 'tertiary',
                   'thoughtful', 'trajectory', 'untapped', 'venture', 'vitality', 'vocalize',
                   'wither']
    
    article_text = article_text.lower().split()
    
    for word in article_text:
        word = nltk.PorterStemmer().stem(word)
        
    for word in status_list:
        word = nltk.PorterStemmer().stem(word)    
    
    for word in article_text:
        if word in status_list:
            status_words += 1        
    
    if len(article_text) > 0:    
        status_score = 1000 * status_words / len(article_text)
        print('Status score is {}.'.format(status_score))
    else:
        print('No text provided to analyze.')
    
main()    
